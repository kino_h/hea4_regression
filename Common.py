__version__ = "0.2.0"

# import seaborn as sns
from pymatgen.core import Element
import plotly.graph_objects as go
import os
import pickle
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import BayesianRidge
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.model_selection import KFold  # , train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
# import time
import numpy as np
import matplotlib.pyplot as plt
import random


def load_save_pickle(filename, df, action=None):
    """load or save DataFrame to filename

    Args:
        filename (str): filename
        df (DataFrame): data
        action (list, optional): to do 'load_object', 'overwrite'. Defaults to None.

    Returns:
        str: real action
        DataFrame: loaded data
    """
    if action is None:
        action = ["load_object"]

    prefix = "pickle"
    loadfilename = os.path.join(prefix,filename)
    if os.path.exists(loadfilename):
        if "overwrite" in action and df is not None:
            with open(loadfilename, "wb") as f:
                pickle.dump(df, f)
            return "saved", df
        if "load_object" in action or df is None:
            with open(loadfilename, "rb") as f:
                df = pickle.load(f)
            return "loaded", df
        return "exists", None
    else:
        if df is not None:
            with open(loadfilename, "wb") as f:
                pickle.dump(df, f)
            return "saved", df
    return "nothing", None


def make_features(feature_groups=["basic_mean_std", "basic_min_max"]):
    """make a list of feature strings

    Args:
        feature_groups (list, optional): descriptor group to add. Defaults to ["basic_mean_std","basic_min_max"].

    Returns:
        list: features
    """
    basic_mean_std = ['group_mean', 'group_std', 'row_mean', 'group_std']
    basic_min_max = ['group_min', 'group_max', 'row_min', 'group_max']

    phys_prop_mean_std = ['X_mean', 'atomic_radius_mean', 'atomic_radius_calculated_mean', 'thermal_conductivity_mean', 'boiling_point_mean', 'melting_point_mean', 'molar_volume_mean', 'log_electrical_resistivity_mean',
                          'group_std', 'row_std', 'X_std', 'atomic_radius_std', 'atomic_radius_calculated_std', 'thermal_conductivity_std', 'boiling_point_std', 'melting_point_std', 'molar_volume_std', 'log_electrical_resistivity_std', ]
    phys_prop_min_max = ['group_min', 'row_min', 'X_min', 'atomic_radius_min', 'atomic_radius_calculated_min', 'thermal_conductivity_min', 'boiling_point_min', 'melting_point_min', 'molar_volume_min', 'log_electrical_resistivity_min',
                         'group_max', 'row_max', 'X_max', 'atomic_radius_max', 'atomic_radius_calculated_max', 'thermal_conductivity_max', 'boiling_point_max', 'melting_point_max', 'molar_volume_max', 'log_electrical_resistivity_max', ]

    group_distribution_mean = ['group1_mean', 'group2_mean',
                               'group3_mean', 'group4_mean', 'group5_mean', 'group6_mean',
                               'group7_mean', 'group8_mean', 'group9_mean', 'group10_mean',
                               'group11_mean', 'group12_mean', 'group13_mean', 'group14_mean',
                               'group15_mean', 'group16_mean', 'group17_mean', 'group18_mean', ]

    row_distribution_mean = ['row1_mean', 'row2_mean', 'row3_mean', 'row4_mean', 'row5_mean',
                             'row6_mean', 'row7_mean', 'row8_mean', 'row9_mean']

    ofv_mean = ['s1_mean', 's2_mean', 'p1_mean',
                'p2_mean', 'p3_mean', 'p4_mean', 'p5_mean', 'p6_mean', 'd1_mean',
                'd2_mean', 'd3_mean', 'd4_mean', 'd5_mean', 'd6_mean', 'd7_mean',
                'd8_mean', 'd9_mean', 'd10_mean', 'f1_mean', 'f2_mean', 'f3_mean',
                'f4_mean', 'f5_mean', 'f6_mean', 'f7_mean', 'f8_mean', 'f9_mean',
                'f10_mean', 'f11_mean', 'f12_mean', 'f13_mean', 'f14_mean']

    features = []
    if "basic_mean_std" in feature_groups:
        features = features + basic_mean_std
    if "basic_min_max" in feature_groups:
        features = features + basic_min_max

    if "phys_prop_mean_std" in feature_groups:
        features = features + phys_prop_mean_std
    if "phys_prop_min_max" in feature_groups:
        features = features + phys_prop_min_max

    if "group_distribution_mean" in feature_groups:
        features = features + group_distribution_mean
    if "row_distribution_mean" in feature_groups:
        features = features + row_distribution_mean
    if "ofv_mean" in feature_groups:
        features = features + ofv_mean

    print("features")
    print(len(features))
    print(features)
    print()
    return features


def make_descriptor(df_features, terms=["X1"]):
    """make original and squared features

    Args:
        df_features (DataFrame): data
        terms (list, optional): ["X1"] or ["X1","X2"]. Defaults to ["X1"].

    Returns:
        np.array: descriptor vector, X
    """
    if "X1" in terms:
        X1 = df_features.values
        X = X1
    if "X1" in terms and "X2" in terms:
        X1 = df_features.values
        X2 = X1**2
        X = np.hstack([X, X2])
    if "X1" in terms and "X2" in terms and "X3" in terms:
        X1 = df_features.values
        X2 = X1**2
        X3 = X1**3
        X = np.hstack([X, X2, X3])
 
    print(X.shape)
    return X


def reg_Xy_all(reg, X, y):
    """fitting and predicting without using trainig and test set

    Args:
        reg (regressor): regressor
        X (np.array): X
        y (np.array): y

    Returns:
        float: R2 value
        np.array: y
        np.array: predicted y
    """
    reg.fit(X, y)
    print("fitting done")
    yt = reg.predict(X)
    r2 = r2_score(y, yt)
    print("R2", r2)
    return r2, y, yt


def reg_Xy_train_test(reg, X, y, label=None):
    """fit and predict with training and test sets

    Args:
        reg (regressor): regressor
        X (np.array): X
        y (np.array): y
        label (np.array, optional): a list of string to specify test y. Defaults to None.

    Returns:
        dict: scores
        np.array: y_test
        np.array: predicted value of y_test
        np.array: label of y_test
    """
    random.seed(0)
    n = y.shape[0]
    train = np.array(random.sample(range(n), int(0.8*n)))
    test = list(set(range(n)) - set(train))
    test = np.array(test)
    Xtrain = X[train]
    ytrain = y[train]
    Xtest = X[test]
    ytest = y[test]
    # Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, test_size=0.2, random_state=0)
    reg.fit(Xtrain, ytrain)
    ytrain_p = reg.predict(Xtrain)
    r2_train = r2_score(ytrain, ytrain_p)
    print("r2(train)", r2_train)

    print("fitting done")
    ytest_p = reg.predict(Xtest)
    r2 = r2_score(ytest, ytest_p)
    mae = mean_absolute_error(ytest, ytest_p)
    rmse = np.sqrt(mean_squared_error(ytest, ytest_p))

    print("R2", r2)
    if label is not None:
        return {"r2": r2, "mae": mae, "rmse": rmse}, ytest, ytest_p, label[test]
    else:
        return {"r2": r2, "mae": mae, "rmse": rmse}, ytest, ytest_p


def reg_Xy_kfold(reg, X, y, n_splits=10, debug=True):
    """fit and predict with k-fold cross validation

    Args:
        reg (regressor): regressor
        X (np.array): X
        y (np.array): y
        n_splits (int, optional): n_splits for KFold. Defaults to 10.
        debug (bool, optional): debug flag. Defaults to True.

    Returns:
        list: a list of R2 value
        np.array: y_test
        np.array: predicted values of y_test
    """
    r2_list = []
    mae_list = []
    rmse_list = []
    ytest_all = []
    ytest_p_all = []
    kf = KFold(n_splits=n_splits, shuffle=True, random_state=0)
    for i, (train, test) in enumerate(kf.split(X)):
        Xtrain, ytrain = X[train], y[train]
        reg.fit(Xtrain, ytrain)
        Xtest, ytest = X[test], y[test]
        ytest_p = reg.predict(Xtest)
        r2 = r2_score(ytest, ytest_p)
        mae = mean_absolute_error(ytest, ytest_p)
        rmse = np.sqrt(mean_squared_error(ytest, ytest_p))

        if debug:
            print(i, "R2", r2)
        r2_list.append(r2)
        mae_list.append(mae)
        rmse_list.append(rmse)
        ytest_all.extend(ytest)
        ytest_p_all.extend(ytest_p)

    ytest_all = np.array(ytest_all)
    ytest_p_all = np.array(ytest_p_all)

    return {"r2": np.mean(r2_list), "mae": np.mean(mae_list), "rmse": np.mean(rmse_list)}, ytest_all, ytest_p_all


def plot_y_yp(ytest, ytest_p, title=None, dir_name="images", show_kde=False, text=None, plot_type=["plt_simple"], alpha=0.2):
    """plot y vs y_predict gplot

    Args:
        ytest (np.array): experimental target variable quantities
        ytest_p (np.array): predicted target variable quantities
        title (str, optional): title of the figure. Defaults to None.
        dir_name (str, optional): directory name to save the figure. Defaults to "images".
        show_kde (bool, optional): a flag to show kde plot, Defaults to False.
    """
    def find_min_max(ytest, ytestp):
        """make minimum and maximum of ytest and ytestp

        Args:
            ytest (np.array): a vector
            ytestp (np.array): another vector

        Returns:
            float: the minium value
            float: the maximum value
        """
        yval = np.hstack([ytest, ytestp])
        minval = np.min(yval)
        maxval = np.max(yval)
        return minval, maxval

    print("number of test data", ytest.shape)
    print("plot_type", plot_type)

    minval, maxval = find_min_max(ytest, ytest_p)

    if "go" in plot_type:
        if text is not None:
            fig = go.Figure(data=[go.Scatter(
                x=ytest,
                y=ytest_p,
                mode='markers',
                text=text,
                marker=dict(size=5, opacity=0.5)
            )]
            )
        else:
            fig = go.Figure(data=[go.Scatter(
                x=ytest,
                y=ytest_p,
                mode='markers',
                marker=dict(size=5, opacity=0.5)
            )]
            )

        fig.update_layout(
            width=800, height=800,
            scene=dict(
                xaxis_title="y_expr",
                yaxis_title="y_pred"))
        fig.show()

    if "plt_detail" in plot_type:

        if text is not None:
            slist = []
            for s in text:
                s = str(s)
                slist.append([int(s[:2]), int(s[2:4]),
                              int(s[4:6]), int(s[6:8])])
            for targetz in [24, 25, 26, 27, 28]:

                fig, ax = plt.subplots(1, 1, figsize=(10, 10))
                if title is not None:
                    ax.set_title(title)
                ax.plot([minval, maxval], [minval, maxval],
                        '--', color="black")
                ax.set_xlabel("$y_{expr}$")
                ax.set_ylabel("$y_{pred}$")
                ax.scatter(ytest, ytest_p, alpha=0.3, s=1)

                ilist = []
                for i, s in enumerate(slist):
                    if targetz in s:
                        ilist.append(i)
                if len(ilist) > 0:
                    ilist = np.array(ilist)
                    symbol = str(Element('H').from_Z(targetz))
                    ax.scatter(ytest[ilist], ytest_p[ilist],
                               alpha=alpha, s=8, label=symbol)

                ax.legend()

                fig.tight_layout()
                ax.set_aspect('equal')

                filename = os.path.join(
                    dir_name, title+"_"+str(targetz)+".png")
                filename = filename.replace(" ", "_")
                fig.savefig(filename)
                fig.show()

            print("image saved: ", filename)

    if "plt_simple" in plot_type:
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        if title is not None:
            ax.set_title(title)
        ax.plot([minval, maxval], [minval, maxval], '--', color="black")
        ax.set_xlabel("$y_{expr}$")
        ax.set_ylabel("$y_{pred}$")
        ax.scatter(ytest, ytest_p, alpha=alpha, s=2)

        fig.tight_layout()
        ax.set_aspect('equal')

        filename = os.path.join(dir_name, title+".png")
        filename = filename.replace(" ", "_")
        fig.savefig(filename)
        fig.show()

        print("image saved: ", filename)


def do_linear_regression(df_feature_target, features, target_name,
                         split="kfold", show_plot=True):
    """fit and predict data by linear regression

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_name (str): a name of the target varible
        score_metric (str): metric of score. Defaults to "r2".

    Returns:
        dict: {title:  score value}
    """
    y = df_feature_target[target_name].astype(float).values

    result_dic = {}

    Xraw = make_descriptor(df_feature_target[features])
    scalerX = StandardScaler()
    X = scalerX.fit_transform(Xraw)
    reg = LinearRegression(fit_intercept=True)
    if split=="train_test":
        score, ytest, ytest_p = reg_Xy_train_test(reg, X, y)
    elif split=="kfold":
        score, ytest, ytest_p = reg_Xy_kfold(reg, X, y)

    title = "linear regression, X1,  {}".format(split)
    result_dic = {title: score}

    if show_plot:
        plot_y_yp(ytest, ytest_p, title=",".join([title, target_name]))

    return result_dic


def do_linear_regresson_X2(df_feature_target, features, target_name,
                           split="kfold", show_plot=True):
    """fit and predict data by linear regression including squared terms of descriptors

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_name (str): a name of the target varible
        score_metric (str): metric of score. Defaults to "r2".

    Returns:
        dict: {title:  score value}
    """
    y = df_feature_target[target_name].astype(float).values

    Xraw = make_descriptor(df_feature_target[features], ["X1", "X2"])
    scalerX = StandardScaler()
    X = scalerX.fit_transform(Xraw)
    reg = LinearRegression(fit_intercept=True)
    if split=="train_test":
        score, ytest, ytest_p = reg_Xy_train_test(reg, X, y)
    elif split=="kfold":
        score, ytest, ytest_p = reg_Xy_kfold(reg, X, y)

    title = "linear regression, X1 X2,  {}".format(split)
    result_dic = {title: score}

    if show_plot:
        plot_y_yp(ytest, ytest_p, title=",".join([title, target_name]))

    return result_dic

def do_linear_regresson_X3(df_feature_target, features, target_name,
                           split="kfold", show_plot=True):
    """fit and predict data by linear regression including squared terms of descriptors

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_name (str): a name of the target varible
        score_metric (str): metric of score. Defaults to "r2".

    Returns:
        dict: {title:  score value}
    """
    y = df_feature_target[target_name].astype(float).values

    Xraw = make_descriptor(df_feature_target[features], ["X1", "X2", "X3"])
    scalerX = StandardScaler()
    X = scalerX.fit_transform(Xraw)
    reg = LinearRegression(fit_intercept=True)
    if split=="train_test":
        score, ytest, ytest_p = reg_Xy_train_test(reg, X, y)
    elif split=="kfold":
        score, ytest, ytest_p = reg_Xy_kfold(reg, X, y)

    title = "linear regression, X1 X2 X3,  {}".format(split)
    result_dic = {title: score}

    if show_plot:
        plot_y_yp(ytest, ytest_p, title=",".join([title, target_name]))

    return result_dic


def do_bayselinear_regresson_X2(df_feature_target, features, target_name,
                                split="kfold", show_plot=True,
                                alpha=None, lambda_=None):
    """fit and predict data by Bayesian linear regression including squared terms of descriptors

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_name (str): a name of the target varible
        score_metric (str): metric of score. Defaults to "r2".

    Returns:
        dict: {title:  score value}
    """
    y = df_feature_target[target_name].astype(float).values

    Xraw = make_descriptor(df_feature_target[features], ["X1", "X2"])
    scalerX = StandardScaler()
    X = scalerX.fit_transform(Xraw)
    #reg = LinearRegression(fit_intercept=True)
    reg = BayesianRidge(tol=1e-6, fit_intercept=True, compute_score=True)
    if alpha is not None:
        reg.set_params(alpha_init=alpha)
    if lambda_ is not None:
        reg.set_params(lambda_init=lambda_)

    if split=="train_test":
        score, ytest, ytest_p = reg_Xy_train_test(reg, X, y)
        print(reg.alpha_)
        print(reg.lambda_)
        print(reg.scores_[-1])
        print("BayesianRidge, {}, alpha {}, gamma {}, score {}".format(target_name, reg.alpha_, reg.lambda_, reg.scores_[-1]))

 
    elif split=="kfold":
        score, ytest, ytest_p = reg_Xy_kfold(reg, X, y)

    title = "Bayesian linear regression, X1 X2,  {}".format(split)
    result_dic = {title: score}

    if show_plot:
        plot_y_yp(ytest, ytest_p, title=",".join([title, target_name]))

    return result_dic


def do_randomforest_regression(df_feature_target, features, target_name,
                               n_jobs=4, n_estimators=100,
                               split="kfold", show_plot=True):
    """fit and predict data by random forest regression

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_name (str): a name of the target varible
        score_metric (str): metric of score. Defaults to "r2".

    Returns:
        dict: {title:  score value}
    """
    y = df_feature_target[target_name].astype(float).values

    Xraw = make_descriptor(df_feature_target[features], ["X1"])
    scalerX = StandardScaler()
    X = scalerX.fit_transform(Xraw)

    reg = RandomForestRegressor(
        n_estimators=n_estimators, oob_score=True, n_jobs=n_jobs)
    if split=="train_test":
        score, ytest, ytest_p = reg_Xy_train_test(reg, X, y)
    elif split=="kfold":
        score, ytest, ytest_p = reg_Xy_kfold(reg, X, y)

    if split != "kfold":
        print("oob score", reg.oob_score_)

    title = "random forest regression, X1,  {}".format(split)
    result_dic = {title: score}

    if show_plot:
        plot_y_yp(ytest, ytest_p, title=",".join([title, target_name]))

    return result_dic


def do_kneighbors_regression(df_feature_target, features, target_name, n_jobs=4,
                             split="kfold", show_plot=True):
    """fit and predict data by k-neighbor regression

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_name (str): a name of the target varible
        score_metric (str): metric of score. Defaults to "r2".

    Returns:
        dict: {title:  score value}
    """

    heakey = df_feature_target["heakey"].values

    y = df_feature_target[target_name].astype(float).values

    Xraw = make_descriptor(df_feature_target[features], ["X1"])
    scalerX = StandardScaler()
    X = scalerX.fit_transform(Xraw)

    reg = KNeighborsRegressor(n_neighbors=100, n_jobs=n_jobs)
    if split=="train_test":
        score, ytest, ytest_p = reg_Xy_train_test(reg, X, y)
    elif split=="kfold":
        score, ytest, ytest_p = reg_Xy_kfold(reg, X, y)

    title = "k neighbors regression, X1,  {}".format(split)
    result_dic = {title: score}

    if show_plot:
        plot_y_yp(ytest, ytest_p, title=",".join([title, target_name]))

    return result_dic
