#!/usr/bin/env python
# coding: utf-8

# In[95]:


#!/usr/bin/env python
# coding: utf-8

# In[150]:
import warnings
import copy
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.linear_model import BayesianRidge, LinearRegression
from sklearn.metrics import r2_score

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from scipy.stats import multivariate_normal
import random
import pandas as pd
from multi_object_scalization import ScalizeTarget
import sys
import os


class BayesianRidgeOpt:
    """Bayesian optimiation using Bayesian Ridge regression
    """

    def __init__(self, df: pd.DataFrame, metadata: dict, scalizetarget: list, 
            terms:list =["X1", "X2"], method: (dict,None)=None, lambda_: float=None):
        """initialization

        Args:
            df (pd.DataFram): data
            metadata (dict): metadata
            scalizetarget (func): function for scalizedtarget
            method (dict, optional): dict of acqusition funciton and optimization. Defaults to None.
            lambda_ (float, optional): initial lambda value of Bayesian Ridge regression. Defaults to None.
        """
        self.df = df.copy()
        self.metadata = copy.deepcopy(metadata)

        self.scalizetarget = scalizetarget

        self.terms = terms

        _method = {'acquisition_function': 'TS', 'optimize': 'min'}
        if method is None:
            self.method = _method
        else:
            _method.update(method)
            self.method = _method

        self.lambda_ = lambda_

    def _fit_predict(self, actions: np.array, target_column: str, alpha=None, lambda_=None):
        """fit and predict

        Args:
            actions (np.array): a list of actions
            target_column (str): target column name
            alpha (float, optional): alpha value of Bayesian Ridge regression. Defaults to None.
            lambda_ (float, optional): lambda vlaue of Bayesian Ridge regression. Defaults to None.

        Returns:
            [type]: [description]
        """
        reg = BayesianRidge(tol=1e-6, fit_intercept=True, compute_score=True)
        if alpha is not None:
            reg.set_params(alpha_init=alpha)
        if lambda_ is not None:
            reg.set_params(lambda_init=lambda_)

        X_train, y_train, X_all, y_all = self.get_Xz(actions, target_column)

        reg.fit(X_train, y_train)
        ymean, ystd = reg.predict(X_all, return_std=True)

        r2 = r2_score(ymean, y_all)

        return ymean, ystd, reg.scores_[-1], r2

    def make_descriptor(self, df_features: pd.DataFrame, terms: list=["X1"]):
        """make original and squared features

        Args:
            df_features (DataFrame): data
            terms (list, optional): ["X1"] or ["X1","X2"]. Defaults to ["X1"].

        Returns:
            np.array: descriptor vector, X
        """
        if "X1" in terms:
            X1 = df_features.values
            X = X1
        if "X1" in terms and "X2" in terms:
            X1 = df_features.values
            X2 = X1**2
            X = np.hstack([X, X2])
        return X


    def get_Xz(self, actions: np.array, target_column: str):
        """get X and z

        Args:
            actions (np.array): a list of action
            target_column (str): a name of target columns in dataframe

        Returns:
            np.array: X training
            np.array: Y training
            np.array: X all
            np.array: Y all
        """
        metadata = self.metadata
        normalized_feature_columns = metadata["normalized_feature_columns"]
        df = self.df
        X_train = self.make_descriptor(df.loc[actions, normalized_feature_columns], self.terms)
        y_train = df.loc[actions, target_column].values
        X_all = self.make_descriptor(df.loc[:, normalized_feature_columns], self.terms)
        y_all = df.loc[:, target_column].values
        return X_train, y_train, X_all, y_all

    def actions(self, actions_taken: list):
        """get next actions

        Args:
            actions_taken (np.array): a list of actions taken already

        Returns:
            np.array: action to take
        """
        zmean_list, zstd_list, score_list = [], [], []
        target_columns = self.metadata["target_columns"]

        for target_column in target_columns:
            # X_train, y_train, X_all = self.get_Xz(actions_taken, target_column)

            if self.lambda_ is not None:
                zmean, zstd, score, r2_score = self._fit_predict(
                    actions_taken, target_column, lambda_=self.lambda_)
            else:
                zmean, zstd, score, r2_score = self._fit_predict(
                    actions_taken, target_column)

            print("r2 {:15} {:.2f}".format(target_column, r2_score))
            zmean_list.append(zmean.tolist())
            zstd_list.append(zstd.tolist())
            score_list.append(score)

        self.score_list = score_list

        zmean, zstd = np.array(zmean_list).T, np.array(zstd_list).T

        zmean_columns = self.metadata["predicted_target_columns"]
        dfpredict = pd.DataFrame(zmean, columns=zmean_columns)

        zstd_columns = self.metadata["predicted_std_target_columns"]
        dfpredict_std = pd.DataFrame(zstd, columns=zstd_columns)

        scalizetarget = self.scalizetarget
        scalized_zmean, _ = scalizetarget.apply(zmean)
        scalized_zmean = scalized_zmean.reshape(-1)
        scalized_zstd = np.sqrt(np.sum(zstd**2, axis=1))
        _df = pd.DataFrame({"scalized_target": scalized_zmean,
                            "scalized_std_target": scalized_zstd})
        self.df = pd.concat([self.df, dfpredict, dfpredict_std, _df], axis=1)

        new_actions, acq_values = self._choose_action(
            scalized_zmean, scalized_zstd)

        del _df
        _df = pd.DataFrame({"acquisition": acq_values})
        self.df = pd.concat([self.df, _df], axis=1)

        return new_actions

    def _choose_action(self, y_test_p_mean: np.array, y_test_p_std: np.array):
        """choose next actions

        Args:
            y_test_p_mean (np.array): the mean value of y
            y_test_p_std (np.array): the stddev values of y

        Returns:
            np.array: a list of actions
            np.array: acquisition function values
        """
        method = self.method

        y_test_p_mean_transformed = y_test_p_mean

        if method['acquisition_function'] == 'TS':
            # Thompson sampling
            n = y_test_p_std.shape[0]
            if False:
                cov = np.diag(y_test_p_std)
                acq = multivariate_normal(
                    mean=y_test_p_mean_transformed, cov=cov, allow_singular=True)
                acq_values = acq.rvs(size=1)
            else:
                distrib = np.random.normal(size=n)
                acq_values = y_test_p_mean_transformed + y_test_p_std*distrib
        elif method['acquisition_function'] == 'LCB':
            acq_values = y_test_p_mean_transformed - y_test_p_std

        index = np.argsort(acq_values)

        choose = method['optimize']
        if choose == "max":
            index = index[::-1]

        return index, acq_values


def add_action(new_actions_list: list, actions: list, N_draw: int=1):
    """auxially function to select actions

    Args:
        new_actions_list (list): a list of next actions
        actions (np.array): actions taken already
        N_draw (int): a number of action to select for each new_actions_list

    Returns:
        [type]: [description]
    """

    print("add_action, Ndraw",N_draw)
    new_actions_add_list = []
    for i, (new_actions) in enumerate(new_actions_list):
        new_actions_add = []
        for g_new_action in new_actions:
            if g_new_action not in actions and g_new_action not in new_actions_add_list:
                new_actions_add.append(g_new_action)
                print(i, "choose", g_new_action)
            if len(new_actions_add) >= N_draw:
                new_actions_add_list.extend(new_actions_add)
                break

    print('add', new_actions_add_list)
    actions = np.hstack([actions, np.array(new_actions_add_list)])
    return actions

def plot_mesh(df, metadata, scalizedtarget):
    target_columns = metadata["target_columns"]
    predicted_target_columns = metadata["predicted_target_columns"]
    predicted_std_target_columns = metadata["predicted_std_target_columns"]

    prefix = "iteration_image"
    for ifig, (target, predicted_target, predicted_std_target) \
        in enumerate(zip(target_columns, predicted_target_columns, predicted_std_target_columns)):
            y = df.loc[:,target].values
            yp = df.loc[:,predicted_target].values
            yp_std = df.loc[:,predicted_std_target].values

            rng = np.random.RandomState(11)
            idx = rng.choice(range(y.shape[0]), 1000)

            yall = np.hstack([y,yp]).reshape(-1)
            lim = (yall.min(), yall.max())

            plt.figure()
            plt.errorbar(y[idx],yp[idx],yerr=yp_std[idx], fmt=".", alpha=0.2)
            plt.axes().set_aspect("equal")
            plt.plot(lim,lim)
            plt.xlim(lim)
            plt.ylim(lim)
            plt.xlabel(target)
            plt.ylabel(predicted_target)
            plt.tight_layout()
            plt.savefig(os.path.join(prefix,target))
            plt.show()

def try_One(df, metadata: pd.DataFrame, actions: list, method_list: dict, scalizedtarget: str,
        terms: list=["X1","X2"], N_draw: int=1):

    # print("actions", actions)

    size = df.shape[0]

    n_target = len(metadata["target_columns"])

    new_actions_list = []
    for method in method_list:
        for lambda_ in [1e-3]:

            opt = BayesianRidgeOpt(df, metadata, scalizetarget=scalizedtarget,
                    terms=terms, method=method, lambda_=lambda_)

            new_actions = opt.actions(actions)
            plot_mesh(opt.df, metadata, scalizedtarget)

            new_actions_list.append(new_actions)

    actions = add_action(new_actions_list, actions, N_draw)

    return actions
