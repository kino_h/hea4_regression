def plot_mesh(df, metadata, scalizedtarget):
    target_columns = metadata["target_columns"]
    predicted_target_columns = metadata["predicted_target_columns"]
    predicted_std_target_columns = metadata["predicted_std_target_columns"]

    for ifig, (target_column, predicted_target, predicted_std_target) \
        in enumerate(zip(target_columns, predicted_target_columns, predicted_std_target_columns)):
            y = df.loc[:,target_columns].values
            yp = df.loc[:,predicted_target].values
            yp_std = df.loc[:,predicted_std_target].values

            plt.figure()
            plt.title(str(ifig))
            plt.bar(y,yp,yp_std)
            plt.xlabel(target_column)
            plt.ylabel(predicted_target_column)
            plt.show()
